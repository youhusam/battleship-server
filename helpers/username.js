class NameHelper {

  constructor() {
    this.names = {}
  }

  claim(name, id) {
    if (!name || this.names[name]) {
      return false;
    } else {
      this.names[name] = id;
      return true;
    }
  }

  getUser(name) {
    if (this.names[name]) {
      return this.names[name];
    } else {
      return null;
    }
  }

  // serialize claimed this.names as an array
  get() {
    let res = [];
    for (let user in this.names) {
      res.push(user);
    }

    return res;
  }

  free(name) {
    if (this.names[name]) {
      delete this.names[name];
    }
  }
}

module.exports = NameHelper;
