class GameRoom {
  constructor(user1, user2) {
    this.users = {};
    this.users[user1] = new GameUser();
    this.users[user2] = new GameUser();
    this.turnFor = user1;
  }

  attack(from, to, coords) {
    if (from === this.turnFor && this.getReadyState()) {
      const attackResult = this.users[to].attack(coords);
      if (attackResult.result === 'miss') this.turnFor = to;
      return attackResult;
    }
    return { result: 'notturn' };
  }

  addShips(forUser, ships) {
    this.users[forUser].addShips(ships);
  }

  setReady(user) {
    this.users[user].ready = true;
  }

  getReadyState() {
    for (let user in this.users) {
      if (!this.users[user].ready) return false;
    }
    return true;
  }
}

class GameUser {
  constructor() {
    this.totalHealth = 8;
    this.board = {}; // coord: ship
    this.ships = { // ship: health
      ship1: 4,
      ship2: 2,
      ship3: 1,
      ship4: 1
    };
    this.ready = false;
    this.attacks = {};
  }

  attack(coords) {
    if (this.attacks[coords]) {
      return { result: 'dup' };
    }
    this.attacks[coords] = true;
    const ship = this.board[coords];
    if (ship) {
      if (--this.totalHealth === 0) {
        const destroyedShip = this.caclulateDestroyedShip(ship, coords);
        return { result: 'win', ship: destroyedShip };
      }

      if (--this.ships[ship.name] === 0) {
        const destroyedShip = this.caclulateDestroyedShip(ship, coords);
        return { result: 'shipdestroyed', ship: destroyedShip };
      }
      return { result: 'hit' };
    }
    return { result: 'miss' };
  }

  caclulateDestroyedShip(ship, coords) {
    const destroyedShip = {};
    if (ship.name === 'ship3' || ship.name === 'ship4')
      destroyedShip[coords] = ship;
    else {
      for (let coord in this.board) {
        if (this.board[coord].name === ship.name) {
          destroyedShip[coord] = this.board[coord];
        }
      }
    }
    return destroyedShip;
  }

  addShips(ships) {
    for (let coord in ships) {
      this.board[coord] = ships[coord];
    }
  }
}

module.exports = GameRoom;
