var express = require('express'),
  routes = require('./routes'),
  http = require('http'),
  path = require('path'),
  morgan = require('morgan'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override');


var app = module.exports = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

app.set('port', process.env.PORT || 4000);
app.use(morgan('dev'));
app.use(bodyParser());
app.use(methodOverride());


if (app.get('env') === 'development') {
  app.use(morgan());
}

app.get('/', routes.index)

const chat = io.of('/chat');
chat.on('connection', require('./routes/chat'));

const game = io.of('/game');
game.on('connection', require('./routes/game'));

server.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
