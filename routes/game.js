const NameHelper = require('../helpers/username');
const GameRoom = require('../helpers/gameRoom');

const nameHelper = new NameHelper();

let gameCount = 0;

const gameRooms = {};

// export function for listening to the socket
module.exports = function (socket) {
  const username = socket.handshake.query.username
  let name = nameHelper.claim(username, socket.id) && username;

  if (!name) {
    socket.emit('init:error', {
      error: 'usertaken'
    });

    socket.disconnect();
  }

  // send the new user their name and a list of users
  socket.emit('init', {
    name: name,
    users: nameHelper.get()
  });

  // notify other clients that a new user has joined
  socket.broadcast.emit('user:join', {
    name: name
  });

  socket.on('disconnect', function () {
    socket.broadcast.emit('user:left', {
      name: name
    });
    nameHelper.free(name);
  });

  socket.on('challenge', (data) => {
    const userId = nameHelper.getUser(data.user);
    if (userId) {
      const gameNumber = gameCount++;
      const roomName = `game-${gameNumber}`;
      socket.join(roomName);
      socket.to(userId).emit('challenge', {
        name: data.name,
        room: roomName,
        from: socket.id
      });
    }
  });

  socket.on('challenge:accepted', (data) => {
    const userId = nameHelper.getUser(data.user);

    gameRooms[data.room] = new GameRoom(data.opponentName, data.user);

    socket.join(data.room);
    socket.to(data.userId).emit('challenge:accepted', data);
    socket.to(socket.id).emit('challenge:accepted', data);
  });

  socket.on('challenge:placeShips', data => {
    gameRooms[data.room].addShips(data.user, data.ships);
  });

  socket.on('challenge:userReady', data => {
    const gameRoom = gameRooms[data.room];
    gameRoom.setReady(data.user);
    if (gameRoom.getReadyState()) {
      socket.in(data.room).emit('challenge:ready', { turnFor: gameRoom.turnFor });
      socket.emit('challenge:ready', { turnFor: gameRoom.turnFor });
    }
  });

  socket.on('challenge:attack', data => {
    const gameRoom = gameRooms[data.room];
    const opponentId = nameHelper.getUser(data.opponentName);
    const attackResult = gameRoom.attack(data.user, data.opponentName, data.coords);

    switch (attackResult.result) {
      case 'miss':
        socket.in(data.room).emit('challenge:attackResult:miss', data);
        socket.emit('challenge:attackResult:miss', data);
        break;
      case 'hit':
        socket.in(data.room).emit('challenge:attackResult:hit', data);
        socket.emit('challenge:attackResult:hit', data);
        break;
      case 'shipdestroyed':
        data.ship = attackResult.ship;
        socket.in(data.room).emit('challenge:attackResult:shipDestroyed', data);
        socket.emit('challenge:attackResult:shipDestroyed', data);
        break;
      case 'win':
        data.ship = attackResult.ship;
        socket.in(data.room).emit('challenge:attackResult:gameOver', data);
        socket.emit('challenge:attackResult:gameOver', data);
        break;
      case 'notturn':
      default:
        socket.in(data.room).emit(`challenge:attackResult:${attackResult.result}`, data);
        socket.emit(`challenge:attackResult:${attackResult.result}`, data);
    }
  });

  socket.on('challenge:declined', data => {
    socket.to(data.userId).emit('challenge:declined', { name: data.name, reason: data.reason });
  });

  socket.on('challenge:quit', data => {
    socket.in(data.room).emit('challenge:quit');
    socket.leave(data.room);
    delete gameRooms[data.room];
  })

};
