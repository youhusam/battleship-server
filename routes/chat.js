let NameHelper = require('../helpers/username');

let nameHelper = new NameHelper();

// export function for listening to the socket
module.exports = function (socket) {
  const username = socket.handshake.query.username
  let name = nameHelper.claim(username, socket.id) && username;

  if (!name) {
    socket.emit('init:error', {
      error: 'usertaken'
    });
    socket.disconnect();
    return;
  }

  // send the new user their name and a list of users
  socket.emit('init', {
    name: name,
    users: nameHelper.get()
  });

  // notify other clients that a new user has joined
  socket.broadcast.emit('user:join', {
    name: name
  });

  // broadcast a user's message to other users
  socket.on('send:message', function (data) {
    socket.broadcast.emit('send:message', {
      user: name,
      text: data.message
    });
  });

  // clean up when a user leaves, and broadcast it to other users
  socket.on('disconnect', function () {
    socket.broadcast.emit('user:left', {
      name: name
    });
    nameHelper.free(name);
  });
};
